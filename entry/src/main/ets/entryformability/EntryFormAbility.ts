import formInfo from '@ohos.app.form.formInfo';
import formBindingData from '@ohos.app.form.formBindingData';
import FormExtensionAbility from '@ohos.app.form.FormExtensionAbility';
import formProvider from '@ohos.app.form.formProvider';

export default class EntryFormAbility extends FormExtensionAbility {

  data: string[] = [
    '红烧肉',
    '清蒸鲈鱼',
    '香辣牛肉面',
    '麻辣火锅',
    '糖醋排骨',
    '梅菜扣肉',
    '宫保鸡丁',
    '西红柿鸡蛋面',
    '酸辣汤',
    '蚝油生菜',
    '番茄炖牛腩',
    '麻婆豆腐',
    '炸酱面',
    '四川担担面',
    '蒜蓉生蚝',
    '冰淇淋披萨',
    '芝士焗大虾',
    '椒盐鸡翅',
    '水煮鱼',
    '炭烤串串香',
    '红烧茄子',
    '京酱肉丝',
    '清蒸鸡',
    '麻辣香锅',
    '蜜汁叉烧',
    '蒜蓉蒸扇贝',
    '黄焖鸡米饭',
    '芒果班戟',
    '牛肉拉面',
    '海鲜炒饭',
    '豆腐脆皮',
    '川味红油炒手',
    '麻辣香蕉炸鸡',
    '海鲜大餐',
    '椒盐虾',
    '脆皮炸鸡',
    '奶油焗龙虾',
    '黑椒牛柳',
    '香煎石斑鱼',
    '梅干菜扣肉',
    '松仁玉米',
    '韭菜鸡蛋饺子',
    '香辣炸鸡翅',
    '榴莲披萨',
    '咖喱鸡饭',
    '葱爆羊肉',
    '辣炒牛肉面',
    '干锅牛蛙',
    '红烧乳鸽',
    '酸辣粉',
    '香辣小龙虾',
    '黑椒蟹',
    '咖啡牛排',
    '蛋炒饭',
    '番茄炒蛋',
    '土豆烧牛肉',
    '虎皮尖椒',
    '火锅牛肉片',
    '鱼香茄子',
    '椒盐脆皮鸭',
    '三杯鸡',
    '苦瓜炒牛肉',
    '干锅花菜',
    '麻辣小龙虾炒年糕',
    '番茄芝士意面',
    '糖醋鲤鱼',
    '牛肉饺子',
    '冰淇淋炸鸡',
    '海鲜炖粉丝',
    '椒盐软壳蟹',
    '蜜汁烤鸭',
    '咖喱牛肉面',
    '青椒肉丝',
    '麻辣牛肉干',
    '黄焖大虾',
    '花生鸡丝凉面',
    '红烧鱼头',
    '脆皮炸毛肚',
    '酸菜鱼',
    '香辣烤鸭翅',
    '豆芽炒肉',
    '甜酸鸡柳',
    '草莓拿破仑',
    '金牌猪扒包',
    '葱油拌面',
    '椒盐土豆片',
    '剁椒鱼头',
    '牛肉粉丝煲',
    '蒜香蒸排骨',
    '韭菜炒鸡蛋',
    '白灼虾',
    '冰镇樱桃番茄沙拉',
    '芋泥馅月饼',
    '酸菜牛肉面',
    '椒盐鱼球',
    '五香卤味',
    '冰淇淋炸薯条',
    '麻辣鸭脖',
    '番茄炖鸡蛋'
  ];

  onAddForm(want) {
    // Called to return a FormBindingData object.
    let formData = {};
    return formBindingData.createFormBindingData(formData);
  }

  onCastToNormalForm(formId) {
    // Called when the form provider is notified that a temporary form is successfully
    // converted to a normal form.
  }

  onUpdateForm(formId) {
    // Called to notify the form provider to update a specified form.
  }

  onChangeFormVisibility(newStatus) {
    // Called when the form provider receives form events from the system.
  }

  onFormEvent(formId, message) {
    let number = this.data.length;

    let formData = {
      'detail': this.data[Math.round(Math.random() * (number - 1))], // 和卡片布局中对应
    };
    let formInfo = formBindingData.createFormBindingData(formData)
    formProvider.updateForm(formId, formInfo).then((data) => {
      console.info('FormAbility updateForm success.' + JSON.stringify(data));
    }).catch((error) => {
      console.error('FormAbility updateForm failed: ' + JSON.stringify(error));
    })
  }

  onRemoveForm(formId) {
    // Called to notify the form provider that a specified form has been destroyed.
  }

  onAcquireFormState(want) {
    // Called to return a {@link FormState} object.
    return formInfo.FormState.READY;
  }
};